/**
 * Pacote responsável por disponibilizar abstrações para prover informações relacionadas ao contexto
 * de controle para informações municipais georreferenciadas.
 *
 * @author Marcio Costa
 *
 */
package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model;