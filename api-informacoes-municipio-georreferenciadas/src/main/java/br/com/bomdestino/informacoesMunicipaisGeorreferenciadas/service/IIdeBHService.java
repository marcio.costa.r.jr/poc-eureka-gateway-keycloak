package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.service;

import java.util.List;

/**
 * Serviço para prover informações relacionadas ao {@link br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.Ide}
 *
 * @author Marcio Costa
 */
public interface IIdeBHService{

    /**
     * Retorna uma lista de infraestrutura de dados espaciais após conversão de dados
     *
     * @return {@link List<br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.Ide>}
     */
    Object buscarInfoIde();

    /**
     * Retorna o ide através do código informado para o mesmo após conversão de dados
     *
     * @param idIde identificador do Ide
     * @return {@link br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.Ide}
     */
    Object buscaPoIdIde(final String idIde);

    /**
     * Retorna o ide através do bairro informado para o mesmo após conversão de dados
     *
     * @param nomeBairro nome do bairro
     * @return {@link br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.Ide}
     */
    Object buscaPorBairro(final String nomeBairro);

}
