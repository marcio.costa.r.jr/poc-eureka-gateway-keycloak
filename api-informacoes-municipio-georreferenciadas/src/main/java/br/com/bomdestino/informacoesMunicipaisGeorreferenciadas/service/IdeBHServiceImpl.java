package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.service;

import br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.Geometria;
import br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.Ide;
import br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.InfoIde;
import br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model.Propriedade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.controller.CatalogoApi.*;

/**
 * Serviço implementado para prover dados relacionados ao IDE BH
 *
 * @author Marcio Costa
 */
@Service
public class IdeBHServiceImpl implements IIdeBHService{

    private static final String FEATURE_IDE = "features";
    private static final String FEATURE_ID_IDE = "id";
    private static final String FEATURE_TYPE_IDE = "type";
    private static final String FEATURE_PROPERTIES_IDE = "properties";
    private static final String FEATURE_GEOMETRY_IDE = "geometry";

    @Override
    public Object buscarInfoIde() {
        return InfoIde.builder().ide(this.getIdes()).build();
    }

    @Override
    public Object buscaPoIdIde(final String idIde) {
        final List<Ide> listaIdeApiFormatada = this.getIdes();
        final Optional<Ide> idePorId = Optional.ofNullable(listaIdeApiFormatada.stream()
                .filter(ide -> ide.getId().equals(idIde)).findFirst()
                .orElse(Ide.builder().build()));

        return InfoIde.builder()
                .ide(Arrays.asList(idePorId.get()))
                .build();
    }

    @Override
    public Object buscaPorBairro(final String nomeBairro) {
        final List<Ide> listaIdeApiFormatada = this.getIdes();
        final Optional<Ide> idePorBairro = Optional.ofNullable(listaIdeApiFormatada.stream().filter(ide
                -> ide.getPropriedade().getNomeBairro().equals(nomeBairro)).findFirst()
                .orElse(Ide.builder().build()));

        return InfoIde.builder()
                .ide(Arrays.asList(idePorBairro.get()))
                .build();
    }

    private List<Ide> getIdes() {
        ResponseEntity<LinkedHashMap> responseEntity = new RestTemplate().getForEntity(ENDERECO_IDE_BH,
                LinkedHashMap.class);
        if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            List<Object> listEmMapIdeFeatures = (List<Object>) responseEntity.getBody().get(FEATURE_IDE);
            return popularIde(listEmMapIdeFeatures);
        }
        return Collections.EMPTY_LIST;
    }

    private List<Ide> popularIde(final List<Object> listaIdeApi) {
        List<Ide> listaIde = new ArrayList<>();
        for (final Object ideObj : listaIdeApi) {
            final LinkedHashMap<String, Object> linkedPropriedadesIde = (LinkedHashMap<String, Object>) ideObj;
            final Object objId = linkedPropriedadesIde.get(FEATURE_ID_IDE);
            final Object objTipo = linkedPropriedadesIde.get(FEATURE_TYPE_IDE);
            final LinkedHashMap<String, Object> objPropriedade = (LinkedHashMap<String, Object>)
                    linkedPropriedadesIde.get(FEATURE_PROPERTIES_IDE);
            final LinkedHashMap<String, Object> objGeometria = (LinkedHashMap<String, Object>)
                    linkedPropriedadesIde.get(FEATURE_GEOMETRY_IDE);

            final Geometria buildGeometria = Geometria.builder()
                    .tipo(objGeometria.get(KEY_TYPE).toString())
                    .coordenadas((List<?>) objGeometria.get(KEY_COORDINATES))
                    .build();
            final Propriedade buildPropriedade = Propriedade.builder()
                    .id(Integer.parseInt(objPropriedade.get(KEY_ID_POP_DMC).toString()))
                    .numeroBairro(Integer.parseInt(objPropriedade.get(KEY_NUM_BAIRRO).toString()))
                    .nomeBairro(objPropriedade.get(KEY_NOME_BAIRRO).toString())
                    .populacao(Integer.parseInt(objPropriedade.get(KEY_POPULACAO).toString()))
                    .domicilio(Integer.parseInt(objPropriedade.get(KEY_DOMICILIO).toString()))
                    .habPorDomicilio(Double.parseDouble(objPropriedade.get(KEY_HAB_DOM).toString()))
                    .habPorKm(Double.parseDouble(objPropriedade.get(KEY_HAB_KM2).toString()))
                    .areaPorKm(Double.parseDouble(objPropriedade.get(KEY_AREA_KM2).toString()))
                    .build();
            final Ide buildIde = Ide.builder()
                    .id(objId.toString())
                    .tipo(objTipo.toString())
                    .geometria(buildGeometria)
                    .propriedade(buildPropriedade)
                    .build();
            listaIde.add(buildIde);
        }
        return listaIde;
    }
}
