/**
 * Pacote responsável por disponibilizar endpoints para prover informações relacionadas ao contexto
 * de controle para informações municipais georreferenciadas.
 *
 * @author Marcio Costa
 *
 */
package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.controller;