package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.service;

/**
 * Interface responsável para viabilizar conversão de dados obtidos via consulta de APIs
 *
 * @author Marcio Costa
 */
public interface IObjetoApi {

    /**
     * Retorna objeto convertido após tratativa de dados da api externa
     *
     * @param obj Objeto a ser convertido
     * @return Objeto convertido
     */
    Object getObjConvertido(Object obj);
}
