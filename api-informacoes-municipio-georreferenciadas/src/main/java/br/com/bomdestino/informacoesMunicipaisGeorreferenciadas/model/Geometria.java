package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Classe responsável por representar a geometria
 *
 * @author Marcio Costa
 */
@Getter
@Setter
@Builder
public class Geometria {

    @JsonProperty("type")
    private String tipo;

    @JsonProperty("coordinates")
    private List<?> coordenadas;
}
