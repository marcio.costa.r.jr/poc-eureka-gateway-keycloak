package br.com.bomdestino.informacoesMunicipaisGeorreferenciadas.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Classe responsável por representar as características de informações da infraestutura de dados espaciais
 *
 * @author Marcio Costa
 */
@Getter
@Setter
@Builder
public class InfoIde {

    @JsonProperty("features")
    private List<Ide> ide;
}
